# dmenu - dynamic menu
* dmenu is an efficient dynamic menu for X.

# Requirements
* In order to build dmenu you need the Xlib header files.
* Fonts needed:
    * JetBrains Mono

# Installation
* Edit config.def.h as needed
* Afterwards enter the following command to build and install dmenu    
    sudo cp config.def.h config.h && make clean install
