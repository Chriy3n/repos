#!/bin/dash

open="$(printf "epr\nmpv\nnvim\nsxiv\nlinks\nwatch\nmount\nunmount\neject\naddlink\nandroidmount\nandroidunmount\nzathura\nlibreoffice\nsetwallpaper\n" | dmenu -n 4 -i -p "What to open with?")"
[ -z "$open" ] && exit 1
if [ $open = "epr" ]
then
	file="$(du -a ~/downloads ~/documents/ | awk '{print $2}' | dmenu -l 32 -i -p "What file")"
	[ -z "$file" ] && exit 1
	$TERMINAL -e $open $file
elif [ $open = "nvim" ]
then
	file="$(du -a ~/downloads ~/.config/ $repoloc/ ~/documents/ | awk '{print $2}' | dmenu -l 32 -i -p "What file")"
	[ -z "$file" ] && exit 1
	$TERMINAL -e $open $file
elif [ $open = "mpv" ]
then
	file="$(du -a ~/downloads ~/videos/ | awk '{print $2}' | grep -e 'mp4' -e 'mov' -e 'mkv'| dmenu -l 32 -i -p "What file")"
	[ -z "$file" ] && exit 1
	$open $file
elif [ $open = "watch" ]
then
    # set dir (directory) as ~/videos/...
    #... = list all files in ~/vidoes/ | clean up the formatting | remove everything before "videos" | take out everything ending in .XXX, XXX being anything (file extentions) | only use directories that have a "/" in them (removes the first level directories, and cleans up whitespace) | change the first "/" into a space for formatting | ask what show you want | change back into proper formatting
    dir="$HOME/videos/$(du -a ~/videos/ | awk '{print $2}' | sed 's/.*videos\///' | grep -v '\....' | grep '/' | sed 's/\// /' | dmenu -i -p "Which show?" | sed 's/ /\//')"
	[ -z "$dir" ] && exit 1
    # list everything in dir (including ".." and ".") | take out ".." (".." stands for the previous directory which we dont want") | change "." to "all" ("." means the current directory, which means all files) | ask which episode we want | turn "all" back into "." (proper formatting)
    file="$(ls -a $dir | grep '\....' | grep -v "\.\." | sed 's/^\.$/all/' | dmenu -i -p "Which episode?" | sed 's/^all$/\./')"
	[ -z "$file" ] && exit 1
	mpv $dir/$file
elif [ $open = "zathura" ]
then
	file="$(du -a ~/downloads ~/documents/ | awk '{print $2}' | grep -e 'pdf' -e 'epub' | dmenu -l 32 -i -p "What file")"
	[ -z "$file" ] && exit 1
	$open $file
elif [ $open = "libreoffice" ]
then
	file="$(du -a ~/downloads ~/documents/ | awk '{print $2}' | grep -e 'doc' -e 'pptx'| dmenu -l 32 -i -p "What file")"
	[ -z "$file" ] && exit 1
	$open $file
elif [ $open = "sxiv" ]
then
	file="$(du -a ~/downloads ~/pictures/ | awk '{print $2}' | grep -e 'jpeg' -e 'png' -e 'jpg' | dmenu -l 32 -i -p "What file")"
	[ -z "$file" ] && exit 1
	$open $file
elif [ $open = "links" ]
then
	open=$BROWSER
	link="$(cat $repoloc/.bin/lib/bookmarks | sed -r '/^\s*$/d' | awk '{print $1}' | dmenu -i -l 32 -p 'bookmarks')"
	[ -z "$link" ] && exit 1
	file="$(cat $repoloc/.bin/lib/bookmarks | grep "$link" | awk '{print $2}')"
	$open $file
elif [ $open = "addlink" ]
then
	name="$(printf "" | dmenu -p "What is the name of this website")"
	[ -z "$name" ] && exit 1
	link="$(printf "" | dmenu -p "What is the link")"
	[ -z "$name" ] && exit 1
	printf "$name $link\n" >> $repoloc/.bin/lib/bookmarks
elif [ $open = "mount" ]
then
    pgrep -x dmenu && exit
    COLS="name,type,size,mountpoint"
    drives="$(lsblk -rpo "$COLS" | awk '$2=="part"&&$4==""{printf "%s (%s)\n",$1,$3}')"
    [ -z "$drives" ] && exit 1
    chosen="$(echo "$drives" | dmenu -i -p "Mount which drive?" | awk '{print $1}')"
    [ -z "$chosen" ] && exit 1
    udisksctl mount -b "$chosen" && pgrep -x dunst && notify-send "$chosen mounted"
elif [ $open = "unmount" ]
then
    pgrep -x dmenu && exit
    COLS="name,type,size,mountpoint"
    drives=$(lsblk -rpo "$COLS" | awk '$2=="part"&&$4!~/boot|home|SWAP/&&length($4)>1{printf "%s (%s) on %s\n",$1,$3,$4}')
    [ -z "$drives" ] && exit
    chosen=$(echo "$drives" | dmenu -i -p "Unmount which drive?" | awk '{print $1}')
    [ -z "$chosen" ] && exit
    udisksctl unmount -b "$chosen" && pgrep -x dunst && notify-send "$chosen unmounted."
elif [ $open = "eject" ]
then
    pgrep -x dmenu && exit
    COLS="name,type,size,mountpoint"
    drives="$(lsblk -rpo "$COLS" | awk '$2=="part"&&$4==""{printf "%s (%s)\n",$1,$3}')"
    [ -z "$drives" ] && exit
    chosen=$(echo "$drives" | dmenu -i -p "Detatch which drive?" | awk '{print $1}')
    [ -z "$chosen" ] && exit
    udisksctl power-off -b "$chosen" && pgrep -x dunst && notify-send "$chosen eject."
elif [ $open = "setwallpaper" ]
then
	setwallpaper.sh
elif [ $open = "androidmount" ]
then
    fusermount -u ~/mnt && notify-send "unmounted successfully"
elif [ $open = "androidunmount" ]
then
    simple-mtpfs ~/mnt && notify-send "mounted successfully"
else
	file="$(du -a ~/downloads ~/videos/ ~/pictures/ ~/.config/ $repoloc/ $repoloc/.bin/ ~/documents/ | awk '{print $2}' | dmenu -l 32 -i -p "What file")"
	[ -z "$file" ] && exit 1
	$open $file
fi
