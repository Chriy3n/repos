#!/bin/sh

case "$(printf "Poweroff\nReboot\nSleep\nLogout\nScreen Lock\nCancel" | dmenu -n 4 -i -p "Choose:")" in
    Cancel) echo "ok" ;;
    Poweroff) systemctl poweroff ;;
    Reboot) systemctl reboot ;;
    Sleep) systemctl suspend ;;
    Screen\ Lock) slock -m "Locked at  $(date "+%a, %b %d %r")" ;;
    Logout) killall xinit ;;
esac
