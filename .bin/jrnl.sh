#!/bin/sh

jrnlname="$HOME/.local/vimwiki/journal/$(date +%b%y| tr '[A-Z]' '[a-z]').wiki"

if [ ! -f $jrnlname ]; then
    echo touch $jrnlname
fi

$TERMINAL -t 'notes' -e nvim -c "norm Go= $(date +%b%e) =" \
    -c "norm Go== $(date +%r) ==" \
    -c "norm Go" \
    -c "norm zz" \
    -c "startinsert" $jrnlname

