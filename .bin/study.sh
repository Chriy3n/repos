#!/bin/bash

case "$1" in
    dis)
        while true
        do pidof -q electron && killall electron
            sleep 10s
        done ;;
    timer)
        studytime=25m
        shortbreak=5m
        longbreak=30m
        
        study=true
        breaks=0
        sessions=0
        
        trap 'notify-send "Great work!"\
                "You completed $sessions session(s)";\
                trap - EXIT; exit' EXIT INT HUP TERM
        
        while [ $study = true ]
        do
                notify-send "Let's do the work!"
                sleep $studytime
                sessions=$((sessions+1))
                study=false
                
                while [ $study = false ]
                do
                        breaks=$((breaks+1))
                        if [ $breaks = 4 ]
                        then
                                notify-send "Well done 4 sessions!"\
                                        "Take a walk"
                                sleep $longbreak
                                study=true
                                breaks=0
                        else
                                notify-send "Way to go!"\
                                        "have a short break"
                                sleep $shortbreak
                                study=true
                        fi
                done
        done
esac
