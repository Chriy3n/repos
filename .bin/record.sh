#!/bin/bash

# Check if ~/videos/recordings/ exists, if not, make it
[ ! -d $HOME/videos/recordings ] && mkdir "$HOME/videos/recordings"
# Checks if ffmpeg is already running
if pidof ffmpeg
# If it is running, then kill it and notify you
then
    killall ffmpeg
    notify-send 'Stopped Recording!' 
# Otherwise start the script
else
# Select screen side, and save it
    slop=$(slop -f "%x %y %w %h")
    read -r X Y W H < <(echo $slop)
# If you dont select anything then cancel
    width=${#W}
    if [ $width -gt 0 ];
    then
        notify-send 'Started Recording!'
# Ask to record with or without audio
        audio="$(printf "Yes\nNo\n" | dmenu -n 0 -i -p "Record with audio?")"
        [ -z "$audio" ] && exit 1
        if [ $audio = "Yes" ]
# Records with pulseaudio default sink
        then
            ffmpeg -f x11grab -s "$W"x"$H" -framerate 60  -thread_queue_size 512  -i $DISPLAY.0+$X,$Y -f alsa -i pulse\
            -vcodec libx264 -qp 18 -preset ultrafast \
            ~/videos/recordings/$(date +%Y%m%d%H%M%S).mp4
        else
# Records with no audio
            ffmpeg -f x11grab -s "$W"x"$H" -framerate 60  -thread_queue_size 512  -i $DISPLAY.0+$X,$Y \
            -vcodec libx264 -qp 18 -preset ultrafast \
        ~/videos/recordings/$(date +%Y%m%d%H%M%S).mp4
        fi
    fi
fi
