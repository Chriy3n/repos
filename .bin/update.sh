#!/bin/sh

dconf dump / > ~/.config/dconf-settings.ini

cp -rv ~/.config/kitty $repoloc/dotfiles/
cp -rv ~/.config/cmus $repoloc/dotfiles/
cp -rv ~/.config/dunst $repoloc/dotfiles/
cp -rv ~/.config/picom $repoloc/dotfiles/
cp -rv ~/.config/sxhkd $repoloc/dotfiles/
cp -rv ~/.config/nvim $repoloc/dotfiles/
cp -rv ~/.config/mpv $repoloc/dotfiles/
cp -rv ~/.config/sxiv $repoloc/dotfiles/
cp -rv ~/.config/htop $repoloc/dotfiles/
cp -rv ~/.config/X11 $repoloc/dotfiles/
cp -rv ~/.config/zsh $repoloc/dotfiles/
cp -rv ~/.config/mimeapps.list $repoloc/dotfiles/
cp -rv ~/.config/user-dirs.dirs $repoloc/dotfiles
cp -rv ~/.bashrc $repoloc/dotfiles/
cp -rv ~/.zshrc $repoloc/dotfiles/
cp -rv ~/.zshenv $repoloc/dotfiles/
cp -rv ~/.config/dconf-settings.ini $repoloc/dotfiles/

pacman -Qqe > $repoloc/dotfiles/pkglist.txt
cd $repoloc/dotfiles/
git status
