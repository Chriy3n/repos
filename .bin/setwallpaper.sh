#!/bin/dash

walldir=~/pictures/wallpapers
folder="$(ls $walldir | dmenu -c -l 32 -i -p "What wallpaper directory?")"
[ -z "$folder" ] && exit 1
wall="$(sxiv -tofb $walldir/$folder/*)"
mode="$(printf "center\ncover\ntile\nfull\nextend\nfill\n" | dmenu -c -l 32 -i -p "What image style?")"
[ -z "$mode" ] && exit 1
hsetroot -$mode $wall
save="$(printf "Yes\nNo\n" | dmenu -c -l 32 -i -p "Save?")"
[ -z "$save" ] && exit 1
if [ $save = "Yes" ]
then
	printf "#!/bin/bash\n\nhsetroot -$mode $wall\n" > $repoloc/.bin/lib/resetwallpaper.sh
fi
