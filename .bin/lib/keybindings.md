# Window Manager Keys
`meta + 1,2,3,q,w,e,a,s,d`  

* Switches the view to workspace (1-9).  
  
`meta + !,@,#,Q,W,E,A,S,D`

* Moves focused window to workspace (1-9).  
  
`meta + ^1,^2,^3,^q,^w,^e,^a,^s,^d`

* Adds focus on workspace (1-9).  
  
`meta + ^!,^@,^#,^Q,^W,^E,^A,^S,^D`

* Adds focused window to workspace (1-9).  
  
`meta + x,X`

* focuses on all windows.  
* sends focused window to all workspaces.  
  
`meta + h,l`

* makes master window smaller.  
* makes master window bigger.  
  
`meta + H,L`

* send focused window to left monitor.  
* send focused window to right monitor.  
  
`meta + j,l`

* focus on the next window  
* focus on the previous window.  
  
`meta + JK`

* push focused window down the stack.  
* push focused window up the stack.  
  
`meta + return`

* push focused window to master stack.  
  
`meta + b,B`

* toggles bar.  
* toggles systemtray.  
  
`meta + -=`

* decrease window gap size.  
* increase window gap size.  
  
`meta + _+`

* decrease how many windows can be in the master stack.  
* increase how many windows can be in the master stack.  
  
`meta + r`

* reloads dwm.  
  
`meta + C`

* closes focused window.  
  
`meta + u,U,^u,i,I,^i`

* set monocle mode.  
* set tiling mode.  
* set focused window to fullscreen.  
* set floating mode.  
* set focused window to floating.  
  
# App Launch Shortcut Keys
`meta + esc,ESC,^ESC`

* launches vimwiki index.  
* launches vimwiki notes.  
* launches vimwiki journal.  
  
``meta + `,~,^~``

* launches vimwiki index.  
* launches vimwiki notes.  
* launches vimwiki journal.  
  
`meta + 4`

* launches primary browser.  
  
`meta + $`

* launches secondary browser.  
  
`meta control + $`

* launches primary broswer in incognito mode.  
  
`meta + 5`

* launches discord.  
  
`meta + %`

* launches pavucontrol.  
  
`meta + 7`

* launches spotify.  
  
`meta + 8`

* launches steam.  
  
`meta + 9,(`

* launches discover overlay.  
* closes discover overlay.  
  
`meta + 0,)`

* launches droidcam.  
* launches mullvad-vpn.  
  
`meta + f,F`

* launches pcmanfm.  
* launches nnn.  
  
`meta + o,O,^O,^o`

* launches dmenu.  
* launches launcher script.  
* launches unicode script.  
* launches clipmenu.  
  
# System Keys
`meta + r`

* reload sxhkd.  
  
`meta + p,P`

* select a size, take a screenshot, save it to ~/pictures/screenshots/, and copy to clipboard and send a notification saying "screenshot captured".  
* take a fullscreen screenshot, save it to ~/pictures/screenshots/, and copy to clipboard and send a notification saying "screenshot captured".  
  
`meta + ^P`

* record screen with record.sh script.  
  
`meta + [,],{,}`

* set the backlight +5%.  
* set the backlight -5%.  
* set the backlight set to 5.  
* set the backlight set to 50.  
  
`meta + ;,',:,"`

* set the volume +5% and update bar.  
* set the volume -5% and update bar.  
* set the volume set to 5 and update bar.  
* set the volume set to 50 and update bar.  
  
`meta + RETURN,RETURN`

* launches terminal (st).  
* sends notification with updatable apps.  
  
`meta + z,Z,^`

* pauses all mpris players, mutes volume, and launches slock with a message of "Locked at $(date)".  
* launches shutdown.sh script.  
* runs slock with no message.  
  
`meta + ,,<,.,>,/,?`

* lowers volume.  
* mutes volume.  
* raises volume.  
* sets volume to 50%.  
* pauses mpris players.  
* launches cmus.  
