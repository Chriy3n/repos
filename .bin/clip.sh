#!/bin/bash
case "$1" in
    trim)
        input=$(find . -maxdepth 1 -name '*.mkv' | fzf)
        [ -z "$input" ] && exit 1
        read -r -e -p "What do you want the output file name: " output
        [ -z "$output" ] && exit 1
        read -r -e -p "First timestamp in hh:mm:ss: " begin
        [ -z "$begin" ] && exit 1
        read -r -e -p "Second timestamp in hh:mm:ss: " end
        [ -z "$end" ] && exit 1

        ffmpeg -i "$input" -ss "$begin" -to "$end" -c:v copy -c:a copy "$output".mkv ;;
    mp4trim)
        input=$(find . -maxdepth 1 -name '*.mkv' | fzf)
        [ -z "$input" ] && exit 1
        read -r -e -p "What do you want the output file name: " output
        [ -z "$output" ] && exit 1
        read -r -e -p "First timestamp in hh:mm:ss: " begin
        [ -z "$begin" ] && exit 1
        read -r -e -p "Second timestamp in hh:mm:ss: " end
        [ -z "$end" ] && exit 1

        ffmpeg -i "$input" -ss "$begin" -to "$end" -c:v copy -c:a copy output.mkv
        ffmpeg -i output.mkv -vf subtitles=output.mkv "$output".mp4
        [ ! -d "$HOME"/.trash ] && mkdir "$HOME/.trash"
        mv -iv output.mkv "$HOME"/.trash
        [ ! -d "$HOME"/videos/clips ] && mkdir "$HOME/videos/clips"
        mv -iv "$output".mp4 "$HOME"/videos/clips ;;
    *)
        printf '%s\n' "trim" "mp4trim" ;;
esac
