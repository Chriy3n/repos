#!/bin/sh
repoloc="$HOME/.local/repos"
printf "Do you want to make home folder lowercase instead of uppercase?\nFor exmample, ~/documents instead of ~Documents\nType y for yes or n for no\n"
read move
if [ $move = "y" ]
then
    mv ~/Desktop/ ~/desktop/
    mv ~/Downloads/ ~/downloads/
    mv ~/Pictures/ ~/pictures/
    mv ~/Templates/ ~/templates/
    mv ~/Documents/ ~/documents/
    mv ~/Music/ ~/music/
    mv ~/Public/ ~/public/
    mv ~/Videos/ ~/videos/
fi

printf "Do you want to clone repos?\nType y for yes or n for no\n"
read repo1
if [ $repo1 = "y" ]
then
    git clone https://gitlab.com/Chriy3n/repos.git $repoloc
fi
printf "Do you want to clone private repos?\nType y for yes or n for no\n"
read repo2
if [ $repo2 = "y" ]
then
    git clone https://gitlab.com/Chriy3n/wiki.git $wikiloc
    git clone https://gitlab.com/Chriy3n/wallpapers.git ~/pictures/wallpapers
fi

printf "Do you want to move dotfiles from repos to .config?\nType y for yes or n for no\n"
read dotfiles
if [ $dotfiles = "y" ]
then
    cp -rv $repoloc/dotfiles/kitty ~/.config/
    cp -rv $repoloc/dotfiles/cmus ~/.config/
    cp -rv $repoloc/dotfiles/dunst ~/.config/
    cp -rv $repoloc/dotfiles/picom ~/.config/
    cp -rv $repoloc/dotfiles/sxhkd ~/.config/
    cp -rv $repoloc/dotfiles/nvim ~/.config/
    cp -rv $repoloc/dotfiles/mpv ~/.config/
    cp -rv $repoloc/dotfiles/sxiv ~/.config/
    cp -rv $repoloc/dotfiles/htop ~/.config/
    cp -rv $repoloc/dotfiles/X11/ ~/.config/
    cp -rv $repoloc/dotfiles/zsh ~/.config/
    cp -rv $repoloc/dotfiles/mimeapps.list ~/.config/mimeapps.list
    cp -rv $repoloc/dotfiles/user-dirs.dirs ~/.config/user-dirs.dirs
    cp -rv $repoloc/dotfiles/.zshrc ~/.zshrc
    cp -rv $repoloc/dotfiles/.zshenv ~/.zshenv
fi

printf "Do you want to restore gnome settings\nType y for yes or n for no\n"
read gnome
if [ $gnome = "y" ]
then
    dconf load / < ~/.config/dconf-settings.ini
fi

printf "Do you want to reinstall the packages listed from $repoloc/dotfiles/pkglist.txt?\nType y for yes or n for no\n"
read dotfiles
if [ $dotfiles = "y" ]
then
    yay -S --needed - < $repoloc/dotfiles/pkglist.txt
fi
