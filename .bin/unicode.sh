#!/bin/sh

chosen=$(cut -d ';' -f1 $repoloc/.bin/lib/unicode | dmenu -p 'emojis' -i | sed "s/ .*//")
[ -z "$chosen" ] && exit
if [ -n "$1" ]; then
	xdotool type "$chosen"
else
	echo "$chosen" | tr -d '\n' | xclip -selection clipboard
	notify-send "'$chosen' copied to clipboard." &
fi
