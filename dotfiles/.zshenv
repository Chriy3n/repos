# Add's ~/.local/bin to $PATH
export PATH="$PATH:$repoloc/.bin:$PATH/.bin/lib"

# Locations for scripts
export repoloc="/home/cab/.local/repos"
export wikiloc="/home/cab/.local/wiki"

# Program settings
export NNN_BMS='a:~/Videos/anime;s:~/Documents/school;t:~/Documents/textbooks;b:~/Documents/books;m:~/Music;p:~/Pictures;S:~/Pictures/screenshots;w:~/Pictures/wallpapers;M:/run/media/cab;D:~/Downloads/'
export NNN_PLUG='i:mediainf;p:preview-tui-ext'
export NNN_FIFO=/tmp/nnn.fifo
export CM_SELECTIONS="primary clipboard"
export CM_DEBUG=0
export CM_OUTPUT_CLIP=1
export MAX_CLIPS=100
export CM_HISTLENGTH=26
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx

# Default Programs
export EDITOR="nvim"
export READER="zathura"
export VISUAL="nvim"
export TERMINAL="st"
export TERMINAL2="alacritty"
export VIDEO="mpv"
export COLORTERM="truecolor"
export OPENER="xdg-open"
export PAGER="less"

# Clean up
export XDOTDIR="$HOME/.config/zsh/zsh"
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export LESSHISTFILE=/dev/null
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export ANDROID_PREFS_ROOT="$XDG_CONFIG_HOME"/android
export CUDA_CACHE_PATH="$XDG_CACHE_HOME"/nv
