" Turns on Syntax Highlighting
syntax on
" Yanks to systen clipboard
set clipboard+=unnamedplus
" Control Backspace to kill word
inoremap <C-H> <C-W>
" Set Leader key
let mapleader = " "
" No error sounds when hitting the bottom
set noerrorbells
" Make it so you can't use your mouse
set mouse-=a
" Highlights whole line
set cursorline
" Indents when you make a code block
set smartindent
" Sets lime numbers
set nu
" Sets the line numbers to be relative to the line you're on
set relativenumber
" Splits open at the bottom and right
set splitbelow splitright
" tabs
set tabstop =4
set softtabstop =4
set shiftwidth =4
set expandtab
" Set treeview
let g:netrw_liststyle = 3
let g:netrw_banner = 0
let g:netrw_winsize = 25
" auto complete stuff
set complete+=kspell
set completeopt=menuone,longest
set shortmess+=c
" php
autocmd BufNewFile,BufRead *.php set filetype=html
" plugins
call plug#begin('~/.local/share/nvim/plugged')
Plug 'https://github.com/morhetz/gruvbox'
Plug 'https://github.com/vim-scripts/AutoComplPop'
Plug 'https://github.com/tpope/vim-surround'
Plug 'https://github.com/alvan/vim-closetag'
Plug 'https://github.com/jiangmiao/auto-pairs'
call plug#end()
" Set's my vim color scheme
colorscheme gruvbox
set background=dark
hi Normal ctermbg=none
" Remap splip navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
" Changes system keyboard yank to control c
map <silent> <C-c> "+y
" Goyo keybinding, centers text and removes all bars
map <silent> <leader>g :Goyo<cr>
" Toggles relative number/ normal numbers
map <silent> <leader>n :set relativenumber!<cr>
" SpellCheck
map <silent> <leader>s :set spell!<cr>
" Makes it so that I can paste correctly
map <silent> <leader>p :set pastetoggle<cr>
" Toggles linewrap
map <silent> <leader>l :set wrap!<cr>
